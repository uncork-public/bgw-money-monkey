import setuptools

setuptools.setup(
    name="moneymonkey",
    version="0.4.0",
    author="Uncork",
    author_email="monkey@uncork.com",
    description="A small utilities package",
    # url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6"
)
