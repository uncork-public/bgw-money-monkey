#!/bin/bash
function printHelp() {
  # display help
  echo "Creates project virtual environment, installs requirements, and activates it. Can also run unit tests and delete environment upon completion." 1>&2
  echo
  echo "Usage: . venv_helper.sh [options...]" 1>&2
  echo "Options:"
  echo "-t, --test    Run pytest unit tests."
  echo "-d, --delete  Delete environment upon completion."
  echo
}

# parse arguments
runTest=false
deleteEnv=false
for var in "$@"; do
  case "$var" in
  "-t" | "--test")
    runTest=true
    ;;
  "-d" | "--delete")
    deleteEnv=true
    ;;
  *)
    printHelp
    case "$(uname -s)" in
    Linux*) return ;;
    Darwin*) exit 2 ;;
    *) exit 1 ;;
    esac
    ;;
  esac
done

# get environment name
scriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
envName=$(basename $scriptDir)

# activate conda shell
condaDir="$(
  cd "$(dirname "$(which conda)")"/..
  pwd
)"

source ${condaDir}/etc/profile.d/conda.sh

echo "[INFO] ENVIRONMENT: $envName"
if [ "$(conda env list | grep $envName)" == "" ]; then
  echo "[INFO] Creating..."
  conda create -y -q -n $envName python=3.7 &>/dev/null
fi

echo "[INFO] Activating..."
conda activate $envName

echo "[INFO] Installing..."
pip install -q -r requirements.txt
pip install -q -r dev_requirements.txt
pre-commit install &>/dev/null

if $runTest; then
  python -m pytest
fi

if $deleteEnv; then
  echo "[INFO] Deleting..."
  conda deactivate
  conda remove -y --name $envName --all &>/dev/null
fi

echo "[INFO] Completed"
