import base64
import json
from io import BytesIO


# Basic utilities


def to_camel(s):
    return "".join(x.title() for x in s.split("_"))


def to_snake(s):
    return "_".join(s.lower().split(" "))


def parse_pubsub_event_attribute(s, attribute):
    return json.loads(base64.b64decode(s).decode("utf-8")).get(attribute)


def parse_match_id(s):
    return parse_pubsub_event_attribute(s, "match_id")


# Google Cloud Storage


def get_object(client, bucket, filename):
    blob = client.get_bucket(bucket).get_blob(filename)

    return blob.download_as_string()


def put_object(client, bucket, filename, data, file_format="parquet"):
    f = BytesIO()
    if file_format == "parquet":
        data.to_parquet(f)
    elif file_format == "feather":
        data.to_feather(f)
    f.seek(0)

    client.get_bucket(bucket).blob(filename).upload_from_file(f)


# Google Pub/Sub


def publish_message(client, project, topic, message):
    client.publish(
        client.topic_path(project, topic), data=json.dumps(message).encode("utf-8"),
    )
